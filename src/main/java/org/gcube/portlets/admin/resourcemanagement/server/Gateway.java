package org.gcube.portlets.admin.resourcemanagement.server;

import java.util.ArrayList;

import com.liferay.portal.model.Group;

public class Gateway {
	private Group site;
	private ArrayList<String> vres;
	public Gateway(Group site, ArrayList<String> vres) {
		super();
		this.site = site;
		this.vres = vres;
	}
	public Group getSite() {
		return site;
	}
	public void setSite(Group site) {
		this.site = site;
	}
	public ArrayList<String> getVres() {
		return vres;
	}
	public void setVres(ArrayList<String> vres) {
		this.vres = vres;
	}
	@Override
	public String toString() {
		return "Gateway [site=" + site + ", vres=" + vres + "]";
	}


}
