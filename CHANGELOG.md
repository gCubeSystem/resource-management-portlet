
# Changelog for gCube Resource Management Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v7.2.0] - 2023-12-15

- Relased due to new common libs in CP

## [v7.1.1] - 2022-06-15

- Relased due to HL portal removal

## [v7.1.0] - 2021-12-27

- New Feature: implemented the locate funcion to indicate the infra gateway through which a VRE is accessible [#22340] 

## [v7.0.0] - 2020-07-21

- Ported to git

- [#19471] Please provide a simple way to get the full Context

## [v6.7.0] - 2019-09-19

Fixes Bug #17462, rmp-common-library: check the source scope when an add or remove operation is performed.

Added D4OS VO to the config file.


## [v6.6.0] - 2018-11-05

[#12726] Infrastructure-monitor: update resource at vo level rather than vre level

## [v6.6.0] - 2018-06-05

Select context menu enabling filtering as you type

Changed codes for AuthZ framework

## [v6.0.0] - 2016-06-28

Ported to Liferay 6.2

Removed Software Upload Widget

## [v5.6.0] - 2015-09-28

Refactored code in dependencies to promote reuse of sweeper features

[#187] Remove scope operation with Report

[#446] Show SmartGearsDistribution version instead of SmartGearsDistributionBundle version for Distro Version


## [v5.4.0] - 2015-04-21

Fixed Ticket #1119 Resource Management / Monitor wrong generic resource query

Ported to GWT 2.7.0

Updated for showing SmartGears versions in gHN Grid View

## [v5.1.0] - 2013-10-21

New Look And Feel

Ported to GWT 2-5-1


Ported to Feather Weight Stack

Removed GCF Dependency

Logging with sl4j Enabled

## [v4.0.0] - 013-02-27

Mavenised

## [v3.0.0] - 2013-01-13

Reengineered completely to support modules

Added one click feature for retrieving resource profiles

generic Resource grid view show description

## [v2.0.0] - 2012-05-04

Completely restyled pinned resources panel (former opened resources panel) using CSS3 and HTML5

Added support for runtime resources editing

Added support for remove from scope

Added automatic show of gHN List at startup with possibility to set it on or off by editing a configuration file

Added automatic highlighting of low memory or disk machines and gHN States
		

## [v1.10.0] - 2012-02-17

Added support for runtime resources removal

Added support for runtime resources scope change

Added sweeper for invalid resources (expired GHNs and orpha RIs)

Ported to GWT 2.2 GXT 2.2.3

## [v1.0.0] - 2010-10-15

irst release on gxt and gwt2.0
